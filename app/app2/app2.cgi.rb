#!/usr/bin/env ruby

require "cgi"
require "json"

cgi = CGI.new

data = JSON.parse(cgi.params["_params"][0])

res = {
  "errors" => [],
  "result" => {
    "a" => 123,
    "data" => data
  }
}

cgi.out(
  type: "application/json",
  charset: "UTF-8"
) do
  JSON.pretty_generate(res)
end
