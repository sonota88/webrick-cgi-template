function puts(...args) {
  console.log(...args);
}

puts("-->> app2");

function run() {
  const req = new Request("/app2/app2");

  const method = "get";
  const data = { aa: 11, bb: 22 };
  const fd = new FormData();
  fd.append("_method", method.toUpperCase());
  fd.append("_params", JSON.stringify(data));

  const fnOk = (result)=>{
    puts("ok", result);
  }

  const fnNg = (errs)=>{
    puts("NG", errs);
  }

  fetch(req, {
    method: 'POST',
    body: fd,
    credentials: 'include', // cookie をリクエストに含める
  }).then((res)=>{
    if (res.ok) {
      puts("res.ok == true", res);
    } else {
      puts("res.ok != true", res);
    }
    return res.json();
  }).then((resData)=>{
    if (resData.errors.length > 0) {
      fnNg(resData.errors);
      return;
    }
    fnOk(resData.result);
  }).catch((err)=>{
    puts(err);
  });
}

window.addEventListener(
  "pageshow",
  run
);
