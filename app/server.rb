require "webrick"

require_relative "common"

TERM_RESET  = "\e[m"
TERM_YELLOW = "\e[0;33m"

def clear_log
  FileUtils.rm_f LOG_PATH
  FileUtils.touch LOG_PATH
end

Thread.new do
  clear_log()
  file = File.open(LOG_PATH)

  loop do
    while line = file.gets
      $stderr.print TERM_YELLOW
      $stderr.puts "STDERR | " + line
      $stderr.print TERM_RESET
    end
    sleep 0.1
  end
end

# --------------------------------

server_config = {
  Port: 8090,
  DocumentRoot: __dir__
}

server = WEBrick::HTTPServer.new(server_config)

Dir.glob("**/*.cgi.rb")
  .each{ |path|
    # foo/bar.cgi.rb => /foo/bar
    mount_path = "/" + path.delete_suffix(".cgi.rb")

    server.mount(
      mount_path,
      WEBrick::HTTPServlet::CGIHandler,
      File.join(__dir__, path)
    )
  }

trap(:INT) { server.shutdown }

server.start
