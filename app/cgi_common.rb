require "webrick/cgi"
require "json"

require_relative "common"

class MyCgi < WEBrick::CGI
  def initialize
    super

    @file = File.open(LOG_PATH, "a")
  end

  def log(*args)
    @file.print Process.pid, " "

    if 2 <= args.size
      args.each_with_index { |arg, i|
        @file.puts "  [#{i}] #{arg}"
      }
    else
      @file.puts args
    end

    @file.flush
  end

  def log_kv(k, v)
    @file.print Process.pid, " "
    @file.puts format("%s (%s)", k, v)
    @file.flush
  end

  def api(req, res)
    log "-->> api_common: api"
    log_kv "_method", req.query["_method"]
    log_kv "_params", req.query["_params"]

    _method = req.query["_method"].downcase.to_sym
    _params = JSON.parse(req.query["_params"])

    status, res_data = yield(_method, _params)

    res.status = status
    res.content_type = "application/json"
    res.body = JSON.pretty_generate(res_data)
  rescue => e
    log e.class, e.message, e.backtrace

    res.status = 500
    errors = [e.inspect]
    res.body = JSON.pretty_generate({ errors: errors })
  end
end
