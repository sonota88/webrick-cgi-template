#!/usr/bin/env ruby

require "cgi"

# # 1.7 GiB
a = []
200000.times {
  # a << ("#{rand}" * 1000)
}
sleep 10

cgi = CGI.new
cgi.out(
  type: "text/html",
  charset: "UTF-8"
) do
  <<~HTML
<!DOCTYPE html>
<html>
<body>
  sample
  <script src="/app1/app1.js"></script>
</body>
</html>
  HTML
end
