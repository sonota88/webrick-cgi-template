#!/usr/bin/env ruby

require_relative "../cgi_common"

class MyCgi
  def _do_get(req, res, _params)
    log "_do_get"
    log_kv "_params", _params
    raise "fdsa" if rand < 0.5

    res_data = {
      "errors" => [],
      "result" => {
        "a" => "app3 get",
        "data" => _params
      }
    }

    [200, res_data]
  end

  def _do_post(req, res, _params)
    log "_do_post"

    res_data = {
      "errors" => [],
      "result" => {
        "a" => "app3 post",
        "data" => _params
      }
    }

    [200, res_data]
  end

  def do_POST(req, res)
    api(req, res) do |_method, _params|
      log "-->> app3 do_POST"

      case _method
      when :get  then _do_get(req, res, _params)
      when :post then _do_post(req, res, _params)
      else
        raise "unsupported method (#{_method.inspect})"
      end
    end
  end
end

MyCgi.new.start
